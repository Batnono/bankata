export class AccountOperation {

    id: number;
    accountId: number;
    type: OperationType;
    amount: number;
    date: Date;

}

export enum OperationType {
    'WITHDRAW',
    'DEPOSIT'
}
