import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  backEndUrl = 'bankAppUrl';

  constructor(private http: HttpClient) { }

  public getBalance(accountId: number): Observable<number> {
    return of(100);
  }

  public deposit(accountId: number, amount: number): Observable<number> {
    const params = new HttpParams();
    params.append('accountId', accountId.toString());
    params.append('amount', amount.toString());
    return this.http.post<number>(this.backEndUrl, {}, {params: params});
  }

  public withdraw(accountId: number, amout: number): Observable<number> {
    return of(0);
  }
}
