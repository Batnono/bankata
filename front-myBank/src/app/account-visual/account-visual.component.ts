import { Component, OnInit } from '@angular/core';
import { AccountService } from '../logic/services/account.service';

@Component({
  selector: 'app-account-visual',
  templateUrl: './account-visual.component.html',
  styleUrls: ['./account-visual.component.scss']
})
export class AccountVisualComponent implements OnInit {

  accountBalance: number;
  operationAmount: number;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.accountService.getBalance(123).subscribe(balance => this.accountBalance = balance);
  }

  deposit(accountId:number, amount: number) {
    this.accountService.deposit(accountId, amount).subscribe(newBalance => this.accountBalance = newBalance);
  }

  withdraw() {}

}
