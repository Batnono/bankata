import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountVisualComponent } from './account-visual.component';

describe('AccountVisualComponent', () => {
  let component: AccountVisualComponent;
  let fixture: ComponentFixture<AccountVisualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountVisualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
