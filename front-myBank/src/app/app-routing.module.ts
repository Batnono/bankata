import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountVisualComponent } from './account-visual/account-visual.component';

const routes: Routes = [
  { path: '', component: AccountVisualComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
