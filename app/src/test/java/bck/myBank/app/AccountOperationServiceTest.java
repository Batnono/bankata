package bck.myBank.app;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import bck.myBank.app.model.AccountOperation;
import bck.myBank.app.model.OperationType;
import bck.myBank.app.repositories.AccountRepository;
import bck.myBank.app.service.AccountService;

@ExtendWith(SpringExtension.class)
public class AccountOperationServiceTest {
    
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void deposit_withPresentAccount_shouldUpdateAccountBalance() {
        assertEquals(this.accountRepository.getById(1234L).getBalance(), 0F);
        AccountOperation operation = this.accountService.deposit(1234L, 1200.05F);
        assertEquals(operation.getOperationAmount(), 1200.05F);
        assertEquals(this.accountRepository.getById(1234L).getBalance(), 1200.05F);
        assertEquals(operation.getAccountId(), 1234L);
        assertEquals(operation.getType(), OperationType.DEPOSIT);
    }

    @Test()
    public void deposit_withNotFoundAccount_shouldThrowNotFoundException() {
        assertThrows(EntityNotFoundException.class, () -> this.accountService.deposit(1234L, 1200.05F));
    }

}
