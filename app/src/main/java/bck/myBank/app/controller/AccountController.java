package bck.myBank.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import bck.myBank.app.model.AccountOperation;
import bck.myBank.app.model.BankAccount;
import bck.myBank.app.service.AccountService;

@RestController
public class AccountController {
    
@Autowired
private AccountService accountService;

@PostMapping
public BankAccount deposit(AccountOperation deposit) {
    return accountService.deposit(deposit.getAccountId(), deposit.getOperationAmount())
}

}
