package bck.myBank.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bck.myBank.app.model.AccountOperation;
import bck.myBank.app.model.BankAccount;
import bck.myBank.app.model.OperationType;
import bck.myBank.app.repositories.AccountRepository;

@Service
public class AccountService {
    
    @Autowired
    private AccountRepository accountRepository;

    public BankAccount deposit(Long accountId, Float amount) {
        BankAccount bankAccount = accountRepository.getById(accountId);
        bankAccount.setBalance(bankAccount.getBalance() + amount);
        bankAccount.addOperation(new AccountOperation(accountId, OperationType.DEPOSIT, amount));
        return bankAccount;
    }


}
