package bck.myBank.app.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import bck.myBank.app.model.AccountOperation;
import bck.myBank.app.model.BankAccount;
import bck.myBank.app.model.OperationType;

@Service
public class AccountRepository {

    private List<BankAccount> accounts = new ArrayList<>();
    
    public AccountOperation deposit(AccountOperation deposit) {
        accounts.get(0).addOperation(deposit);
        accounts.stream()
        .filter(account -> account.getId().equals(deposit.getAccountId()))
        .findFirst().get().addOperation(deposit);
        return new AccountOperation(1234L, OperationType.DEPOSIT, 1200f);
    }

    public BankAccount getById(long id) throws EntityNotFoundException {
        return this.accounts.stream().filter(acct -> acct.getId() != null && acct.getId().equals(id)).findFirst().orElseThrow(EntityNotFoundException::new);
    }

}
