package bck.myBank.app.model;


import lombok.Data;

@Data
public class AccountOperation {
    
   
    private Long id;
    private Long accountId;
    private OperationType type;
    private Float operationAmount;

    public AccountOperation(Long accountId, OperationType deposit, Float amount) {
    }


}