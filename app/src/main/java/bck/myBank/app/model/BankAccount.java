package bck.myBank.app.model;

import java.util.List;

import javax.persistence.Entity;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.Generated;

@Data
@Component
@Entity
public class BankAccount {
    
    @Generated
    private Long id;

    private Float balance;

    private List<AccountOperation> operations;

    public void addOperation(AccountOperation accountOperation) {
        this.operations.add(accountOperation);
    }
}
