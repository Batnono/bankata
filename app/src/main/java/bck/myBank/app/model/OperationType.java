package bck.myBank.app.model;

public enum OperationType {
    WITHDRAW,
    DEPOSIT
}